# webmention-static-ci

Sending Webmention to several endpoints
* brid.gy
* webmention.app
* directly

### Running
* git clone https://gitlab.com/the-foundation/webmention-static-ci.git /tmp/webment
* `bash /tmp/webment/run.sh /tmp/persist/ https://yoursite.lan/sitemap.xml`