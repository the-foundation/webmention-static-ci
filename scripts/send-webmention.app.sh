#!/bin/bash

## 2023_03 : webmention.app seems to be deleted

test -e /tmp/.webmention_crawl_cache || mkdir /tmp/.webmention_crawl_cache

which curl &>/dev/null || (
    which apt-get &>/dev/null && apt-get update &>/dev/null && apt-get install -y --no-install-recommends curl && apt-get clean all  ;
    which apk     &>/dev/null && apk add curl   &>/dev/null ;
)

NEW="$2"
sum=$(echo $NEW|md5sum|cut -d" " -f1)
echo $(
test -e $1/$sum || ( 
    echo "WEBMENTION.APP:CHECK:$NEW" ; 
    curl -s -X GET "https://webmention.app/check?url=$NEW" |grep -e DEPLOYMENT_NOT_FOUND -e '"error":true'|| ( 
        curl -s -X POST "https://webmention.app/check?url=$NEW" && touch $1/$sum ) 
    )
)
test -e $1/$sum || exit 1
test -e $1/$sum && exit 0

## thanks to https://gist.github.com/dianoetic/b45466a7c04fa47cf80905b182dbda3c
