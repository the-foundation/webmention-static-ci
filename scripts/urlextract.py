import requests
from bs4 import BeautifulSoup
import sys
from urllib.parse import urlparse



 
#url = 'https://the-foundation.gitlab.io/posts/0000_soundtrack/'
url = 'sys.argv.1'
indomain = urlparse(url).netloc
#print(domain) # --> www.example.test
reqs = requests.get(url)
soup = BeautifulSoup(reqs.text, 'html.parser')
 
urls = []
for link in soup.find_all('a'):
    outdomain = urlparse(link).netloc
    if(outdomain not indomain):
        print(link.get('href'))
