#!/bin/bash

## as long as the brig.gy site contains image tags like src="/oauth_dropins_static/blogger_2x.png"
## get platform list          :  curl https://brid.gy/ -s |grep oauth_dropi |grep png|cut -d/ -f3|cut -d "_" -f1|grep -v -e bootstr -e util  |sed 's/^/ /g'|tr -d '\n' |sed 's/^ //g'
## get platform grep patterns :  curl https://brid.gy/ -s |grep oauth_dropi |grep png|cut -d/ -f3|cut -d "_" -f1|grep -v -e bootstr -e util  |sed 's/^/ -e /g'|tr -d '\n'
[[ -z "BRIDGY_PLATFORMS" ]] && BRIDGY_PLATFORMS="twitter facebook instagram flickr github mastodon reddit tumblr wordpress blogger medium"

#test -e /tmp/.webmention_crawl_cache    || mkdir  /tmp/.webmention_crawl_cache
test -e /tmp/.brigy_site_not_registered/ || mkdir /tmp/.brigy_site_not_registered/
test -e /tmp/.brigy_site_not_registered/ && find /tmp/.brigy_site_not_registered/ -type f -mmin +15 -delete

send_bridgy_curl() {
  CHECKS_OK=yes
  [[ -z "$1" ]] && CHECKS_OK=no
  [[ -z "$2" ]] && CHECKS_OK=no
  [[ -z "$3" ]] && CHECKS_OK=no
  ## platform filter
  echo "$3" |grep -q  -e twitter -e facebook -e instagram -e flickr -e github -e mastodon -e reddit -e tumblr -e wordpress -e blogger -e medium || CHECKS_OK=no
  [[ "$CHECKS_OK" = yes ]] && {
    LOCKDIR="$1"
    NEW="$2"
    TARGET="$3"
    sum=$(echo $NEW|md5sum|cut -d" " -f1)
    DOMAIN=$(echo "$TARGET"|cut -d"/" -f3)

    SHORT="SHORTCODE_NOT_FOUND" ;

    echo "$TARGET" |grep -e wordpress           && SHORT=WORDP ;
    echo "$TARGET" |grep -e flickr              && SHORT=FLCKR ;
    echo "$TARGET" |grep -e flickr              && SHORT=FLCKR ;
    echo "$TARGET" |grep -e blogger             && SHORT=BLOGR ;
    echo "$SHORT"|grep "SHORTCODE_NOT_FOUND" -q && SHORT=$(echo "$TARGET" |head -c 5 |tr "[:lower:]" "[:upper:]" )


    test -e "$LOCKDIR/$TARGET.$sum"  || test -e "/tmp/.brigy_site_not_registered/$TARGET.$DOMAIN" || (
        ## THE REAL THINGY TO SEND BRID.GY REQUESTS
        res=$(curl -s -H "Content-Type: application/x-www-form-urlencoded" --request POST \
        -d source="$NEW" \
        -d target="https://brid.gy/publish/$platform" \
        "https://brid.gy/publish/webmention" )
        LOGPREFIX="BRID.GY:$SHORT:CHECK:$NEW"
        echo "$res" |grep -q -e Error -e error -e ERROR || (
            ## NO ERROR FOUND
            echo "$res"  > "$LOCKDIR/$TARGET.$sum" 
        )
        echo "$res" |grep -q -e Error -e error -e ERROR || (
            echo "$LOGPREFIX :" "$res"
        )
        echo "$res" |grep -q '"error": "Could not find' | grep "account for"  -q && touch  "/tmp/.brigy_site_not_registered/$TARGET.$DOMAIN"
    ) 
  } # END CHECKS_OK
} # END send_bridgy_curl

for platform in $(echo $BRIDGY_PLATFORMS );do 
    send_bridgy_curl "$1" "$2" "$platform"
done

LOCKDIR="$1"
NEW="$2"
TARGET="$3"
sum=$(echo $NEW|md5sum|cut -d" " -f1)
for platform in $(echo $BRIDGY_PLATFORMS );do 
    test -e $LOCKDIR/$TARGET.$sum || exit 1
done


#test -e $LOCKDIR/twitter.$sum && test -e $LOCKDIR/mastodon.$sum && exit 0
#exit 1


## thanks to https://gist.github.com/dianoetic/b45466a7c04fa47cf80905b182dbda3c


## EXAMPLE CALL

###echo $(
###test -e $LOCKDIR/mastodon.$sum || (echo "BRID.GY:MAST:CHECK:$NEW";
###curl -s -H "Content-Type: application/x-www-form-urlencoded" --request POST \
###-d source="$NEW" \
###-d target="https://brid.gy/publish/mastodon" \
###"https://brid.gy/publish/webmention" | grep error || touch  $LOCKDIR/mastodon.$sum )
###
