#!/bin/bash
test -e "$1" || exit 1
test -e /tmp/.webmention_crawl_cache || mkdir /tmp/.webmention_crawl_cache

which whim &>/dev/null || (
  which cpanm &>/dev/null  || (
    which apt-get   &>/dev/null && (
        apt-get update && apt-get install -y --no-install-recommends libnet-ssleay-perl cpanminus && apt-get clean all
    )
    which apk   &>/dev/null && (
        apk add cpanm
    )
  )
  test -e /etc/scripts || mkdir /etc/scripts
  test -e /etc/scripts/whim && (
    cd /etc/scripts/whim && git pull &>/dev/null
  )
  test -e /etc/scripts/whim || (
    git clone https://github.com/jmacdotorg/whim.git /etc/scripts/whim
  
  )
  cd /etc/scripts/whim
  cpan LWP::Protocol::https IO::Socket::SSL Net::SSL
  _CPANM_OPTS="--notest --mirror https://cpan.metacpan.org/ --mirror http://mirrors.ibiblio.org/CPAN/ --mirror http://mirror.cogentco.com/pub/CPAN/";cpanm --quiet --showdeps Whim|   xargs -n 1 -P $( nproc ) cpanm ${_CPANM_OPTS}
  cpanm --installdeps .
  perl Makefile.PL
  make -j $(nproc)
  make install
)

link="$2"
sum=$(echo $NEW|md5sum|cut -d" " -f1)
NEW="$link"
test -e $1/$sum || {
URLLOCK=$( echo "$NEW"|sed 's/[^a-zA-Z0-9\.\-]/_/g'|sed 's/_\+/_/g')
SOURCE=${BASH_SOURCE[0]}
while [ -L "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
python3 $DIR/urlextract.py |while read target;do

LINKLOCK=$( echo "$target"|sed 's/[^a-zA-Z0-9\.\-]/_/g'|sed 's/_\+/_/g' )

res=$(whim send "$NEW" "$target")
echo "$res"|grep -q '^Webmention sent' &&  ( touch "$1/${URLLOCK}_${LINKLOCK}"; echo "$res" > "$1/${URLLOCK}_${LINKLOCK}" )
echo "WEBMENTION.WHIM: $res FOR SOURCE $NEW TARGET: $URL"

done

exit 0 ; } ;

exit 0


