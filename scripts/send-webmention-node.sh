#!/bin/bash
test -e /tmp/.webmention_crawl_cache || mkdir /tmp/.webmention_crawl_cache

which git &>/dev/null || (which apt-get && apt-get update &>/dev/null && apt-get install -y --no-install-recommends curl git nodejs npm  && apt-get clean all  ; which apk &>/dev/null && apk add git npm nodejs curl )
which npm &>/dev/null || (which apt-get && apt-get update &>/dev/null && apt-get install -y --no-install-recommends curl git nodejs npm  && apt-get clean all  ; which apk &>/dev/null && apk add git npm nodejs curl )

test -e /etc/scripts || mkdir /etc/scripts
test -e /etc/scripts/.gitub.com_drivet_send-all-webmentions && (
  cd /etc/scripts/.gitub.com_drivet_send-all-webmentions && git pull &>/dev/null
)
test -e /etc/scripts/.gitub.com_drivet_send-all-webmentions || (
  git clone https://github.com/drivet/send-all-webmentions.git /etc/scripts/.gitub.com_drivet_send-all-webmentions
  cd /etc/scripts/.gitub.com_drivet_send-all-webmentions
  npm install
)
link="$2"
sum=$(echo $NEW|md5sum|cut -d" " -f1)
NEW="$link"
test -e $1/$sum || {
echo "WEBMENTION.SELF:SEND:$NEW"
  (  
    cd /etc/scripts/.gitub.com_drivet_send-all-webmentions ;
    cat index.js;
    echo '(async () => {';
    echo ' const URL = "'$link'"; const report = await sendAllWebmentions(URL); console.log(`${JSON.stringify(report)}`); })(); ' 
  ) > run.$sum.js
res=$(node run.$sum.js 2>&1 )
rm run.$sum.js
echo "$res"|grep -q -e error -e errno -e DEPLOYMENT_NOT_FOUND || ( touch $1/$sum; echo "$res" > $1/$sum )
echo "$res"|grep -q -e error -e errno -e DEPLOYMENT_NOT_FOUND && echo "$res"
echo "$res"|grep -q -e error -e errno -e DEPLOYMENT_NOT_FOUND && exit 1
exit 0 ; } ;
