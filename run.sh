#!/bin/bash

install_packages() {
  [[ "$1" = "PACKAGE_INSTALLER" ]] && echo "INSTALLER"
  PKG_APK="make git npm nodejs curl libc-dev gcc python3-dev libxslt-dev py3-pip python3  py3-libxml2 perl-app-cpanminus apkbuild-cpan perl-json perl-yaml perl-net-ssleay perl-dbd-sqlite perl-lwp-protocol-https perl-lwp-useragent-determined perl-xml-xpath perl-moox-types-mooselike perl-moox-types-mooselike-numeric composer php-xmlwriter php-tokenizer php-xml php-dom php-json php-curl icu-data-full perl-test2-suite perl-sub-info perl-test-deep perl-ipc-run perl-test-warn perl-test-output perl-namespace-autoclean perl-class-tiny perl-file-copy-recursive perl-test-without-module  perl-module-pluggable perl-term-table perl-sub-quote perl-html-tree perl-test-requiresinternet perl-datetime-timezone perl-class-inspector perl-datetime-format-builder perl-test-needs perl-perlio-utf8_strict perl-datetime perl-datetime-calendar-julian perl-datetime-format-builder perl-datetime-format-iso8601 perl-datetime-format-mail perl-datetime-format-natural perl-datetime-format-pg perl-datetime-format-strptime perl-datetime-format-w3cdtf perl-datetime-format-xsd perl-datetime-hires perl-datetime-locale perl-datetime-timezone perl-moosex-types-datetime perl-moo perl-moose perl-moosex perl-moosex-types perl-moosex-types-common perl-moosex-types-datetime perl-moosex-types-path-class perl-moosex-types-uri perl-moox-types-mooselike perl-moox-types-mooselike-numeric perl-mojolicious perl-digest-sha1 perl-test-warnings " 
  PKG_DEB="make curl git python3 python3-libxml2 python3-lxml python3-pip python3-wheel cargo cpanminus perl liblwp-protocol-https-perl libnet-ssleay-perl libyaml-perl libdatetime-format-sqlite-perl libdbd-sqlite3-perl libclass-dbi-sqlite-perl libxml-xpathengine-perl libxml-dom-xpath-perl libmoox-struct-perl libmoox-traits-perl libmoox-aliases-perl libmoox-options-perl libjson-perl libtest-simple-perl libtest-xml-simple-perl libdbd-sqlite3-perl composer"
  #nodejs npm 
  NEED_STUFF=no
  #npm
  for need in trafilatura git curl whim composer ;do 
   which $need &>/dev/null |grep -q  "$need" || NEED_STUFF=yes 
   [[ "$1" = "PACKAGE_INSTALLER" ]] && which "$need"
  done
  #|sed 's/^/ /g' |tr -d '\n'
  [[ "$1" = "PACKAGE_INSTALLER" ]] && echo "INSTALL:" $NEED_STUFF
  [[ "$NEED_STUFF" = "yes" ]] && (
      which apt-get 2>/dev/null |grep /apt-get && ( 
        ( apt-get update &>/dev/null && apt-get install -y --no-install-recommends $PKG_DEB 2>&1  && apt-get clean all && pip3 install trafilatura 2>&1  |grep -v -e "Requirement already satisfied" -e "is already the newest" -e "Reading package lists"  -e "Building dependency tree" -e " It is recommended to use a virtual environment instead" ; )
      ) ## end deb
      which apk 2>/dev/null   |grep /apk && (
         apk add $PKG_APK && pip3 install trafilatura  2>&1 |grep -v -e "Requirement already satisfied" -e "is already the newest" -e "Reading package lists"  -e "Building dependency tree"  -e " It is recommended to use a virtual environment instead"
         apk add perl-xml-dom &>/dev/null
      )    ## end apk
      test -e /etc/scripts || mkdir /etc/scripts


      ##
      ###end php
   ) # end need stuff
    test -e /etc/scripts/webmentions-php/mention-client-php/vendor/autoload.php || (
          git clone --recurse-submodules https://gitlab.com/the-foundation/webmention_send_client.php.git /etc/scripts/webmentions-php
    )
    test -e  /etc/scripts/webmentions-php/mention-client-php/vendor/autoload.php ||   ( 
          cd /etc/scripts/webmentions-php/mention-client-php/ ; 

          md5sum /etc/scripts/webmentions-php/mention-client-php/composer.json
            (
               
               php --version|grep -q "PHP 8.1" && echo 'ewogICJuYW1lIjogImluZGlld2ViL21lbnRpb24tY2xpZW50IiwKICAiZGVzY3JpcHRpb24iOiAiQ2xpZW50IGxpYnJhcnkgZm9yIHNlbmRpbmcgd2VibWVudGlvbiBhbmQgcGluZ2JhY2sgbm90aWZpY2F0aW9ucyIsCiAgInR5cGUiOiAibGlicmFyeSIsCiAgImxpY2Vuc2UiOiAiQXBhY2hlLTIuMCIsCiAgImhvbWVwYWdlIjogImh0dHBzOi8vZ2l0aHViLmNvbS9pbmRpZXdlYi9tZW50aW9uLWNsaWVudC1waHAiLAogICJhdXRob3JzIjogWwogICAgewogICAgICAibmFtZSI6ICJBYXJvbiBQYXJlY2tpIiwKICAgICAgImVtYWlsIjogImFhcm9uQHBhcmVja2kuY29tIiwKICAgICAgImhvbWVwYWdlIjogImh0dHA6Ly9hYXJvbnBhcmVja2kuY29tLyIKICAgIH0KICBdLAogICJhdXRvbG9hZCI6IHsKICAgICJwc3ItMCI6IHsKICAgICAgIkluZGllV2ViIjogInNyYy8iCiAgICB9CiAgfSwKICAibWluaW11bS1zdGFiaWxpdHkiOiAiZGV2IiwKICAicmVxdWlyZSI6IHsKICAgICJwaHAiOiAiPj01LjYiLAogICAgIm1mMi9tZjIiOiAiZGV2LW1haW4iCiAgfSwKICAicmVxdWlyZS1kZXYiOiB7CiAgICAicGhwdW5pdC9waHB1bml0IjogImRldi1tYWluIiwKICAgICJwaHB1bml0L3BocC1jb2RlLWNvdmVyYWdlIjogImRldi1tYWluIgogIH0KfQo=' |base64 -d |tee  /etc/scripts/webmentions-php/mention-client-php/composer.json  |md5sum 
               php --version|grep -q "PHP 8.0" && echo 'ewogICJuYW1lIjogImluZGlld2ViL21lbnRpb24tY2xpZW50IiwKICAiZGVzY3JpcHRpb24iOiAiQ2xpZW50IGxpYnJhcnkgZm9yIHNlbmRpbmcgd2VibWVudGlvbiBhbmQgcGluZ2JhY2sgbm90aWZpY2F0aW9ucyIsCiAgInR5cGUiOiAibGlicmFyeSIsCiAgImxpY2Vuc2UiOiAiQXBhY2hlLTIuMCIsCiAgImhvbWVwYWdlIjogImh0dHBzOi8vZ2l0aHViLmNvbS9pbmRpZXdlYi9tZW50aW9uLWNsaWVudC1waHAiLAogICJhdXRob3JzIjogWwogICAgewogICAgICAibmFtZSI6ICJBYXJvbiBQYXJlY2tpIiwKICAgICAgImVtYWlsIjogImFhcm9uQHBhcmVja2kuY29tIiwKICAgICAgImhvbWVwYWdlIjogImh0dHA6Ly9hYXJvbnBhcmVja2kuY29tLyIKICAgIH0KICBdLAogICJhdXRvbG9hZCI6IHsKICAgICJwc3ItMCI6IHsKICAgICAgIkluZGllV2ViIjogInNyYy8iCiAgICB9CiAgfSwKICAibWluaW11bS1zdGFiaWxpdHkiOiAiZGV2IiwKICAicmVxdWlyZSI6IHsKICAgICJwaHAiOiAiPj01LjYiLAogICAgIm1mMi9tZjIiOiAiPj0wLjIiCiAgfSwKICAicmVxdWlyZS1kZXYiOiB7CiAgICAicGhwdW5pdC9waHB1bml0IjogIl44IgogIH0KfQo=' |base64 -d |tee  /etc/scripts/webmentions-php/mention-client-php/composer.json  |md5sum 


               );
            md5sum /etc/scripts/webmentions-php/mention-client-php/composer.json
            (cd /etc/scripts/webmentions-php/mention-client-php/;

            #touch composer.lock ;
            composer install || cat composer.json ) ;
          )
    test -e  /etc/scripts/webmentions-php/mention-client-php/vendor/autoload.php || exit 1
    which whim   2>/dev/null  |grep /whim || ( 
        test -e /etc/scripts/whim && (
          cd /etc/scripts/whim && git pull &>/dev/null
        )
        test -e /etc/scripts/whim || (
          git clone https://github.com/jmacdotorg/whim.git /etc/scripts/whim

        )
        which cpan &>/dev/null && cpan LWP::Protocol::https IO::Socket::SSL Net::SSL
        _CPANM_OPTS="--notest --mirror https://cpan.metacpan.org/ --mirror http://mirrors.ibiblio.org/CPAN/ --mirror http://mirror.cogentco.com/pub/CPAN/";cpanm --quiet --showdeps Whim|   xargs -n 1 -P $( nproc ) cpanm ${_CPANM_OPTS}
        cd /etc/scripts/whim
        cpanm --installdeps .
        perl Makefile.PL
        make -j $(nproc)
        make install
      ) ## end whim

}

install_packages
[[ "$1" = "PACKAGE_INSTALLER" ]] && exit 0
[[ "$2" = "PACKAGE_INSTALLER" ]] && exit 0

LOCKDIR=$1
SITEMAP=$2

test -e "$LOCKDIR" || echo "LOCKDIR MISSING"
test -e "$LOCKDIR" || exit 1

## restore git time 
test -e "$LOCKDIR/.git" && (
  cd "$LOCKDIR"
  rev=HEAD
  for f in $(git ls-tree -r -t --full-name --name-only "$rev") ; do
      touch -d $(git log --pretty=format:%cI -1 "$rev" -- "$f") "$f" & sleep 0.05 ;
  done
wait
)

echo "$SITEMAP"|grep '^/' && {              test -e "$SITEMAP" || echo "FILE $SITEMAP NOT FOUND" ; } ; 
echo "$SITEMAP"|grep '^/' && {              test -e "$SITEMAP" || exit 2                         ; } ;
echo "$SITEMAP"|grep '^/' || { (echo "$SITEMAP"|grep ^http -q) || echo "non http_s sitemap"      ; } ;
echo "$SITEMAP"|grep '^/' || { (echo "$SITEMAP"|grep ^http -q) || exit 3                         ; } ;

echo "WEBMENTION_SEND:START"

bash -c "test -e  $LOCKDIR/webmentions.sent    || mkdir $LOCKDIR/webmentions.sent"
bash -c "test -e  $LOCKDIR/bridgy.sent         || mkdir $LOCKDIR/bridgy.sent"
bash -c "test -e  $LOCKDIR/webmention.app.sent || mkdir $LOCKDIR/webmention.app.sent"

mylinklist=$(trafilatura --sitemap "$SITEMAP" --list |grep -v -e "$url/page" -e "$url/404.html" |sort -u)
SOURCE=${BASH_SOURCE[0]}
while [ -L "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR=$( cd -P "$( dirname $SOURCE ) " >/dev/null 2>&1 && pwd ) # " syntax highlight fails without this comment single quote
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )  # " syntax highlight fails without this comment single quote
cd $DIR

#(echo "$mylinklist"| bash -c 'while read link;do sum=$(echo $link|md5sum|cut -d" " -f1);test -e '$LOCKDIR'/bridgy.sent/$sum         || bash '${DIR}/scripts'/send-webmention-bridgy.sh  '$LOCKDIR'/bridgy.sent         "$link" & sleep 0.5; done; wait' ) &
#(echo "$mylinklist"| bash -c 'while read link;do sum=$(echo $link|md5sum|cut -d" " -f1);test -e '$LOCKDIR'/webmention.app.sent/$sum || bash '${DIR}/scripts'/send-webmention.app.sh     '$LOCKDIR'/webmention.app.sent "$link" & sleep 0.5; done; wait' ) &
#(echo "$mylinklist"| bash -c 'while read link;do sum=$(echo $link|md5sum|cut -d" " -f1);test -e '$LOCKDIR'/webmentions.sent/$sum    || bash '${DIR}/scripts'/send-webmention-node.sh    '$LOCKDIR'/webmentions.sent    "$link" & sleep 0.5; done; wait' ) &
test -e /tmp/.webmention_crawl_cache || mkdir /tmp/.webmention_crawl_cache
echo "$mylinklist"|grep -v ^$|while read inlink;do 
    sum=$(echo "$inlink"|md5sum|cut -d" " -f1|cut -f1)
    echo -e '\r';
    echo -n "$sum |STATUS: "; 
    retcode=$(
      curl -A "Firefox" \
    --retry 2 --retry-delay 30 --retry-max-time 90 --happy-eyeballs-timeout-ms 423 --retry-connrefused \
        -o "/tmp/.webmention_crawl_cache/$sum" -w "%{http_code}" "$inlink" -L -c -s
    )
    echo -n "$retcode"
    rawpage=$( cat "/tmp/.webmention_crawl_cache/$sum"  )
## CHECK FOR FAILS
    failed="no"
    echo "$retcode" |grep -e ^3 -e ^4 -e ^5 && rawpage=""
    echo "$retcode" |grep -e ^3 -e ^4 -e ^5 && failed="yes"
    echo "$retcode" |grep -e ^3 -e ^4 -e ^5 && (
      echo -n "FAIL_BLANKING"
      echo "" > "/tmp/.webmention_crawl_cache/$sum"
    )
    (echo "$failed"|grep -q "no" ) && grep \
                                      -e "The access policies of a site define which visits are allowed. Your current visit is not allowed according to those policies." \
                                      -e "You are unable to access this website. Please contact us if the problem persists." \
                                      -e "Your request has been blocked. This may be due to several reasons" \
                                      -e 'Access Denied.*Sucuri Website Firewall error'  \
                                      -e 'The owner of this website.*has banned your IP' \
                                      -e 'Please complete the security check to access' \
                                      -e 'needs to review the security of your connection before proceeding'                                       "/tmp/.webmention_crawl_cache/$sum" && echo -n " POSSIBLY_BLOCKED "
    (echo "$failed"|grep -q "no" ) && (
    echo -n " |CSUM: "$( cat   "/tmp/.webmention_crawl_cache/$sum"|md5sum )
    echo -n " |SIZE: "$( du -k "/tmp/.webmention_crawl_cache/$sum" )
    )

    (echo "$failed"|grep -q "no") && (echo "$rawpage"|grep -q -e  'link rel="webmention"' -e  "link rel='webmention'" ) && {
      test -e $LOCKDIR/bridgy.sent/$sum            || bash ${DIR}/scripts/send-webmention-bridgy.sh          $LOCKDIR/bridgy.sent                  "$inlink" & 
      test -e $LOCKDIR/webmention.app.sent/$sum    || bash ${DIR}/scripts/send-webmention.app.sh             $LOCKDIR/webmention.app.sent          "$inlink" & 
#                                                      bash ${DIR}/scripts/send-webmention-whim.sh    $LOCKDIR/webmentions.whim.sent    "$inlink" & 
#      test -e $LOCKDIR/webmentions.node.sent/$sum  || bash ${DIR}/scripts/send-webmention-node.sh    $LOCKDIR/webmentions.node.sent    "$inlink" & 
#      test -e $LOCKDIR/webmentions.rust.sent/$sum  || bash ${DIR}/scripts/send-webmention-rust.sh    $LOCKDIR/webmentions.ruse.sent    "$inlink" & 
    } 
    echo "$rawpage"|grep -q -e "rel='pingback'" -e 'rel="pingback"' -e  'link rel="webmention"' -e  "link rel='webmention'" && {
    export WEBMENTION_CACHE_DIR="$LOCKDIR/webmentions.php.sent"
                                                       bash ${DIR}/scripts/send-webmention-php.sh    cachedir=$LOCKDIR/webmentions.php.sent  target="$inlink" cached_source_file="/tmp/.webmention_crawl_cache/$sum" & 
    }
    ## end webmention
done
sleep 3;

for m in $(seq 1 4);do 
  sleep 5;
      (
        ( test -e "$LOCKDIR/.git" && cd "$LOCKDIR" && git add -A && git commit -m "added locks" && git push ) |tr -d '\n';echo
      ) |sed 's/^/AUTOPUSH:/g'
done
wait
test -e "$LOCKDIR/.git" && cd "$LOCKDIR" && git add -A && git commit -m "added locks" && git push
echo "WEBMENTION:SEND:DONE , SUCCESSFUL RESULTS IN $LOCKDIR"

#    - trafilatura --sitemap "https://www.sitemaps.org/sitemap.xml" --list|wc -l
#    - bash -c 'cat /tmp/url.out.list|while read link;do sum=$(echo $link|md5sum|cut -d" " -f1);test -e /tmp/persist/bridgy.sent/$sum        || bash $myrepo/send-bridgy.sh           /tmp/persist/bridgy.sent         "$link" & sleep 0.5; done; wait'
#    - bash -c 'cat /tmp/url.out.list|while read link;do sum=$(echo $link|md5sum|cut -d" " -f1);test -e /tmp/persist/webmention.app.sent/$sum || bash $myrepo/send-webmention.app.sh  /tmp/persist/webmention.app.sent "$link" & sleep 0.5; done; wait'
#    - bash -c 'cat /tmp/url.out.list|while read link;do sum=$(echo $link|md5sum|cut -d" " -f1);test -e /tmp/persist/webmentions.sent/$sum || bash $myrepo/send-webmention.node.sh    /tmp/persist/webmentions.sent    "$link" & sleep 0.5; done; wait'
#    - bash -c ' url="https://the-foundtaion.gitlab.io" && trafilatura --sitemap $url/sitemap.xml --list |grep -v -e "$url/page" -e "$url/404.html" |sort -u > /tmp/url.out.list '
